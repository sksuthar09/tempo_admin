<div class="sidebar-menu">
	<div class="sidebar-header">
		<div class="logo">
			<h4>Fikrgo</h4>
		</div>
	</div>
	<div class="main-menu">
		<div class="menu-inner">
			<nav>
				<ul class="metismenu" id="menu">
					<li class="{{ request()->is(['/','home']) ? 'active' : '' }}">
						<a href="/home" aria-expanded="true"><i class="ti-dashboard"></i><span>Dashboard</span></a>
					</li>
					<li class="{{ Request::is(['tempo_enquiry','tempo_enquiry/*']) ? 'active' : '' }}">
						<a href="/tempo_enquiry" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Vehicle Enquiry</span></a>
					</li>
					<li class="{{ Request::is(['live-trip','live-trip/*']) ? 'active' : '' }}">
						<a href="/live-trip" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Live Trip</span></a>
					</li>
					<li class="{{ request()->is(['Driver_Details/driver_profile','Driver_Details/driver_profile/*']) ? 'active' : '' }}">
						<a href="/Driver_Details/driver_profile" aria-expanded="true"><i class="ti-pie-chart"></i><span>Driver Profile</span></a>
					</li>
					<li class="{{ request()->is(['trip-search','trip-search/*']) ? 'active' : '' }}">
						<a href="{{ url('trip-search') }}" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Trip Search</span></a>
					</li>
					<li class="{{ request()->is(['trip-confirm','trip-confirm/*']) ? 'active' : '' }}">
						<a href="{{ url('trip-confirm') }}" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Trip Confirm</span></a>
					</li>					
					<li class="{{ request()->is(['driver-payout','driver-payout/*']) ? 'active' : '' }}">
						<a href="{{ url('driver-payout') }}" aria-expanded="true"><i class="ti-pie-chart"></i><span>Driver Payout</span></a>
					</li>
					<li class="{{ request()->is(['Tempo/tempo','Tempo/tempo/*']) ? 'active' : '' }}">
						<a href="/Tempo/tempo" aria-expanded="true"><i class="ti-palette"></i><span>Tempo</span></a>
					</li>
					<li class="{{ request()->is(['Driver_List/driver_list','Driver_List/driver_list/*']) ? 'active' : '' }}">
						<a href="/Driver_List/driver_list" aria-expanded="true"><i class="ti-slice"></i><span>Driver List</span></a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</div>