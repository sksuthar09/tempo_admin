@extends('layouts.app')

@section('content')
<div class="main-content-inner">
<div class="col-lg-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Driver List</h4>
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table table-hover text-center">
                                         <a href="/Driver_List/add_driver" class="btn btn-secondary">ADD Driver</a> 
                                            <thead class="text-uppercase">
                    <tr>
                    <th>Id</th>
                     <th>Name</th>
                     <th>Momile Number</th>
                     <th>Address</th>
                     <th>Email</th>
                     <th>Tempo Type</th>
                     <th>Contact Date</th>
                    </tr>
                    </thead>
                    @foreach($data as $key=>$value)
                    <tbody>
                     <tr>
                     <td> {{$value->id}}</td>
                     <td> {{$value->name}}</td>
                     <td> {{$value->mobile_number}}</td>
                     <td> {{$value->address}}</td>
                     <td>{{$value->email}}</td>
                     <td>{{$value->tempo_type}}</td>
                     <td>{{$value->contact_date}}</td>
                   </tbody>
                   @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
               @endsection


