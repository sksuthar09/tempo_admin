@extends('layouts.app')

@section('content')
<div class="main-content-inner">
<div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h3 class="card-title">Add Driver</h3>
                    <p class="card-description ">  </p>
                    <form class="forms-sample " method="POST">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputName1">Name</label>
                        <input type="text"  class="form-control" name="name" placeholder="Name" required="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Mobile Number</label>
                        <input type="text"    class="form-control" name="mobile_number" placeholder="Mobile Number" required="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Email address</label>
                        <input type="email" required="" class="form-control" name="email" placeholder="Email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Address</label>
                        <input type="text" required="" class="form-control" name="address" placeholder="Address">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Tempo Type</label>
                        <select class="form-control" name="tempo_type" required="">
                                <option>Tata Ace</option>
                                <option>Three Wheeler</option>
                                <option>Pickup 8FT</option>
                              </select>
                       </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Contact Date</label>
                        <input type="text" required="" class="form-control" name="contact_date" placeholder="DD/MM/YYYY">
                      </div>
                     <button type="submit" class=" submit btn btn-dark mr-2">Submit</button>
                   </form>
                  </div>
                </div>
              </div>
</div>
@endsection

 
           

