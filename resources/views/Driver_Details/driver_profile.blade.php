@extends('layouts.app')

@section('content')
<div class="main-content-inner">
   <div class="col-lg-12 mt-5">
      <div class="card">
         <div class="card-body">
            <h4 class="header-title">Driver Profile</h4>
            <div class="single-table">
               <div class="table-responsive">
                  <table class="table text-center">
                     <thead class="text-uppercase">
                        <tr>
                           <th>Id</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Mobile Number</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     @foreach($data as $key=>$value)
                     <tbody>
                        <tr>
                           <td>{{$value->id}}</td>
                           <td>{{$value->user->name}}</td>
                           <td>{{$value->user->email}}</td>
                           <td>{{$value->user->mobile_number}}</td>
                           <td>{!! $value->status=='1' ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>' !!}</td>
                           <td>
                              <a href='{{ url("Driver_Details/driver_profile/$value->id") }}' class="btn btn-sm btn-primary">view</a>
                           </td>
                        </tr>
                     </tbody>
                     @endforeach
                  </table>
                  {{ $data->links() }}
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
