@extends('layouts.app')

@section('content')
<div class="page-title-area">
   <div class="row align-items-center">
      <div class="col-sm-12">
         <div class="breadcrumbs-area clearfix">
            <h4 class="page-title pull-left">Driver Profile</h4>
            <ul class="breadcrumbs pull-left">
               <li><a href="{{ url('/') }}">Home</a></li>
               <li><a href="{{ url('/Driver_Details/driver_profile') }}">Driver Profile</a></li>
               <li><span>{{$data->id}}</span></li>
            </ul>
         </div>
      </div>      
   </div>
</div>
<div class="main-content-inner">
   <div class="content-wrapper">
   <div class="row">
      <div class="col-lg-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <h6 class="header-title">Driver details</h6>
               <div class="single-table">
                  <div class="table-responsive">
                     <table class="table table-bordered mb-0">
                        <tr>
                           <td><strong>Name</strong></td>
                           <td>{{$data->user->name}}</td>
                        </tr>
                        <tr>
                           <td><strong>Mobile number</strong></td>
                           <td>{{$data->user->mobile_number}}</td>
                        </tr>
                        <tr>
                           <td><strong>Email</strong></td>
                           <td>{{$data->user->email}}</td>
                        </tr>
                        <tr>
                           <td><strong>Adhar No</strong></td>
                           <td>{{$data->adhar_card_no}}</td>
                        </tr>                      
                        <tr>
                           <td><strong>Address</strong></td>
                           <td>{{$data->address}}</td>
                        </tr>
                        <tr>
                           <td><strong>City</strong></td>
                           <td>{{$data->city}}</td>
                        </tr>
                        <tr>
                           <td><strong>Pincode</strong></td>
                           <td>{{$data->pincode}}</td>
                        </tr>
                        <tr>
                           <td><strong>Status</strong></td>
                           <td>{!! $data->user->status=='1' ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>' !!}</td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @if($data->linkOwnerDriver)
      <div class="col-lg-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <h6 class="header-title">Owner details</h6>
               <div class="single-table">
                  <div class="table-responsive">
                     <table class="table table-bordered mb-0">
                        <tr>
                           <td><strong>Name</strong></td>
                           <td>{{$data->linkOwnerDriver->owner->name}}</td>
                        </tr>
                        <tr>
                           <td><strong>Mobile number</strong></td>
                           <td>{{$data->linkOwnerDriver->owner->mobile_number}}</td>
                        </tr>
                        <tr>
                           <td><strong>Email</strong></td>
                           <td>{{$data->linkOwnerDriver->owner->email}}</td>
                        </tr>
                        <tr>
                           <td><strong>Name</strong></td>
                           <td>{{$data->user->name}}</td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
   </div>

   <div class="row"> 
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body" >
               <h6 class="header-title">Driver ID Images</h6>
               <div style="display: flex;">
                   <div>
                     <img src="{{url($data->id_front_img)}}">
                  </div>
                  <div>
                     <img src="{{url($data->id_back_img)}}">
                  </div>  
               </div>                 
            </div>
         </div>
      </div>
   </div>
   <div class="row"> 
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body" >
               <h6 class="header-title">RC Images</h6>
               <div style="display: flex;">
                   <div>
                     <img src="{{url($data->vehicleDetails->rc_front_img)}}">
                  </div>
                  <div>
                     <img src="{{url($data->vehicleDetails->rc_back_img)}}">
                  </div>  
               </div>                 
            </div>
         </div>
      </div>
   </div>
    <div class="row"> 
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body" >
               <h6 class="header-title">Driving Licence</h6>
               <div style="display: flex;">
                   <div>
                     <img src="{{url($data->vehicleDetails->rc_front_img)}}">
                  </div>
                  <div>
                     <img src="{{url($data->vehicleDetails->rc_back_img)}}">
                  </div>  
               </div>                 
            </div>
         </div>
      </div>
   </div>
   @if($data->linkOwnerDriver)
    <div class="row"> 
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body" >
               <h6 class="header-title">Owner ID details</h6>
               
               <div style="display: flex;">
                   <div>
                     <img src="{{url($data->vehicleDetails->owner_id_front_img)}}">
                  </div>
                  <div>
                     <img src="{{url($data->vehicleDetails->owner_id_back_img)}}">
                  </div>  
               </div>                 
            </div>
         </div>
      </div>
   </div>
   @endif
   <div class="row"> 
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <h6 class="header-title">Vehicle details</h6>
               <div class="single-table">
                  <div class="table-responsive">
                     <table class="table table-bordered mb-0">
                        <tr>
                           <td><strong>Vehicle type : </strong></td>
                           <td>{{$data->vehicleDetails->vehicle->name}}</td>
                           <td><strong>Vehicle No : </strong></td>
                           <td>{{$data->vehicleDetails->vehicle_number}}</td>
                        </tr>
                        <tr>
                           <td><strong>Own vehicle : </strong></td>
                           <td></td>
                           <td><strong>Drive the vehicle : </strong></td>
                           <td></td>
                        </tr>
                        <tr>
                           <td><strong>Vehicle Insurance from : </strong></td>
                           <td>{{$data->vehicleDetails->insurance_from}}</td>
                           <td><strong>Vehicle Insurance to : </strong></td>
                           <td>{{$data->vehicleDetails->insurance_to}}</td>
                        </tr>
                        <tr>
                           <td><strong>Date reg : </strong></td>
                           <td>{{$data->vehicleDetails->reg_date}}</td>
                           <td><strong>Reg validity : </strong></td>
                           <td>{{$data->vehicleDetails->red_validity}}</td>
                        </tr>  
                        <tr>
                           <td><strong>Engine no : </strong></td>
                           <td>{{$data->vehicleDetails->engine_no}}</td>
                           <td><strong>Vehicle class : </strong></td>
                           <td>{{$data->vehicleDetails->vehicle_class}}</td>                           
                        </tr>                       
                        <tr>
                           <td><strong>Fuel type : </strong></td>
                           <td>{{$data->vehicleDetails->fual_used}}</td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row"> 
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <h6 class="header-title">Bank details</h6>
               <div class="single-table">
                  <div class="table-responsive">
                     <table class="table table-bordered mb-0">
                        <tr>
                           <td><strong>Beneficiary Name : </strong></td>
                           <td>{{$data->bankDetails->beneficiary_name}}</td>
                           <td><strong>Beneficiary Bank : </strong></td>
                           <td>{{$data->bankDetails->bank->name}}</td>
                        </tr>
                        <tr>
                           <td><strong>Beneficiary Account no : </strong></td>
                           <td>{{$data->bankDetails->beneficiary_account_no}}</td>
                           <td><strong>Beneficiary IFSC : </strong></td>
                           <td>{{$data->bankDetails->beneficiary_ifsc}}</td>
                        </tr>
                        <tr>
                           <td><strong>Beneficiary Branch : </strong></td>
                           <td>{{$data->bankDetails->beneficiary_branch}}</td>                         
                        </tr>                        
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection

