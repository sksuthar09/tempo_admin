@extends('layouts.app')

@section('content')
<div class="main-content-inner">
<div class="col-lg-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Tempo</h4>
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table table-hover text-center">
                                            <thead class="text-uppercase">
                                                <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            @foreach($data as $key=>$value)
                                            <tbody>
                                                <tr>
                                                <td> {{$value->id}}</td>
                                                <td> {{$value->name}}</td>
                                                <td> <img src="{{ url($value->image) }}" ></td>
                                                <td>  
                                                <?php if ($value->status == '1') : ?>
                                                <span class="badge badge-success">Active</span>
                                                <?php else  : ?>
                                                <span class="badge badge-danger">Inactive</span>    
                                                <?php endif ?>
                                                </td>
                                                <td>
                                                <a href='{{ url("Tempo/tempo/$value->id") }}' class="btn btn-secondary">view</a>
                                                </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
              @endsection