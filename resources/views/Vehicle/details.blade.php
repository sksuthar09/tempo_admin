@extends('layouts.app')

@section('content')
<div class="main-content-inner">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-lg-8 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">						 
				<table class="table col-lg-4">
				@foreach($data as $key=>$value)
				 <tr><th>{{$value->name}}</th></tr>
				 <tr><td class="height=350px">
				 <img src="{{ url($value->image) }}"> 
				 </td>
				 <td>
				 <table>
						 <tr><td><strong> Capacity</strong></td><td>{{$value->capacity}}</td></tr>
						 <tr><td><strong>Size</strong> </td><td>{{$value->size}}</td></tr>
						 <tr><td><strong>Start Fare</strong> </td><td>{{$value->start_fare}}</td></tr>
						 <tr><td><strong>Status</strong> </td><td>
						 <?php if ($value->status == '1') : ?>
						 <span class="badge badge-success">Active</span>
                           <?php else  : ?>
						 <span class="badge badge-danger">Inactive</span>
                           <?php endif ?>
						 </td></tr>
						 <tr><td> 
						 <?php if ($value->status == '0') : ?>
								<button type="button" id='1' class="eventButton btn btn-success btn-fw" value="{{$value->id}}" >Active</button>
								<?php endif ?>
								<?php if ($value->status == '1') : ?>
								<button type="button" id='0' class="eventButton btn btn-danger btn-fw"  value="{{$value->id}}">Inactive</button>
								<?php endif ?></td></tr>
						 </td></tr>
				</table>
				</td></tr>
			 @endforeach
			</table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
	$('.eventButton').click(function(){
	//Selected value
	var inputValue = $(this).attr("id");
	var id = $(this).attr("value");
	alert("Are you sure "+inputValue);

	$.ajax({
				url:"{{ url('test') }}",
				type: 'POST',
				dataType: 'JSON',
				data : { 'status':inputValue,'id':id,  "_token": "{{ csrf_token() }}"},
				success:function(resp)
				{
				if(resp.status==1)
					{
						window.location.href = "{{ url('Tempo/tempo') }}";}
						else
						{window.location.href = "{{ url('Tempo/tempo') }}";
					}
				}
			});
		});
	});
</script>
           
           