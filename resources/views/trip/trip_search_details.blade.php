@extends('layouts.app')

@section('content')
<div class="page-title-area">
   <div class="row align-items-center">
      <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Trip Search</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ url('') }}">Dashboard</a></li>
                    <li><a href="{{ url('trip-search') }}">Trip Search</a></li>
                    <li><span>{{ $data['id'] }}</span></li>
                </ul>
            </div>
      </div>
      <div class="col-sm-6 clearfix">
            
      </div>
   </div>
</div>
<div class="main-content-inner">
	<div class="card mt-3">
      <div class="card-body">
       <table class="table table-bordered mb-0">
      		<tr>
      			<td><strong>Request Id # :</strong></td>
      			<td>{{ $data->id }}</td>
      			<td><strong>Enquiry Date: </strong></td>
      			<td>{{$data->created_at->format('d-m-Y H:i')}}</td>
      			<td><strong>Status: </strong></td>
      			<td>
      				@if($data->status=='booked')
         				<span class="badge badge-success">{{ucfirst($data->status)}}</span> 			
         			@elseif($data->status=='cancelled')
         				<span class="badge badge-danger">{{ucfirst($data->status)}}</span>
         			@elseif($data->status=='pending')
         				<span class="badge badge-warning">{{ucfirst($data->status)}}</span>
         			@endif
         		</td>
      		</tr>      		
      	</table>
      </div>
   </div>
</div>
<div class="main-content-inner">
   <div class="card">
      <div class="card-body">      	
         <h5 class="header-title">Trip Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Name :</strong></td>
                        <td>{{$data->user->name}}</td>
                        <td><strong>Mobile No :</strong></td>
                        <td>{{$data->user->mobile_number}}</td>                        
                     </tr>
                     <tr>
                        <td><strong>IP Address :</strong></td>
                        <td>{{$data->current_ip}}</td>                  
                        <td></td>                  
                        <td></td>                  
                     </tr>                                  
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-body">         
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>
                        <td style="width: 200px;"><strong>From Place :</strong></td>
                        <td>{{$data->from_place}}</td>                  
                        <td style="width: 200px;"><strong>To Place :</strong></td>                  
                        <td>{{$data->to_place}}</td>                  
                     </tr>                                  
                     <tr>                        
                        <td><strong>From Lattitude :</strong></td>
                        <td>{{$data->from_lat}}</td>
                        <td><strong>From Longitude :</strong></td>
                        <td>{{$data->from_long}}</td>                        
                     </tr>
                     <tr>                        
                        <td><strong>To Lattitude :</strong></td>
                        <td>{{$data->to_lat}}</td>
                        <td><strong>To Longitude :</strong></td>
                        <td>{{$data->to_long}}</td>                        
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-body">         
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>
                        <td><strong>Receiver Name :</strong></td>
                        <td>{{$data->receiver_name}}</td>                  
                        <td><strong>Receiver Number :</strong></td>                  
                        <td>{{$data->receiver_number}}</td>                  
                     </tr>                                  
                     <tr>                        
                        <td><strong>Vehicle Type :</strong></td>
                        <td>{{$data->vehicle->name}}</td>
                        <td><strong>Goods Type :</strong></td>
                        <td>{{$data->goodsType->name}}</td>                        
                     </tr>                     
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-body">         
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>
                        <td><strong>Amount :</strong></td>
                        <td>{{$data->amount}}</td>
                        <td><strong>Amount Type :</strong></td>           
                        <td>{{ucfirst($data->payment_type)}}</td>
                     </tr>              
                     <tr>
                        <td><strong>Coupon Code :</strong></td>
                        <td>{{$data->coupon_code}}</td>
                        <td><strong></strong></td>
                        <td></td>      
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection