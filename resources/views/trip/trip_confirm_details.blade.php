@extends('layouts.app')

@section('content')
<div class="page-title-area">
   <div class="row align-items-center">
      <div class="col-sm-12">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Trip Confirm</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ url('') }}">Dashboard</a></li>
                    <li><a href="{{ url('trip-search') }}">Trip Confirm</a></li>
                    <li><span>{{ $data->id }}</span></li>
                </ul>
            </div>
      </div>      
   </div>
</div>
<div class="main-content-inner">
	<div class="card mt-3">
      <div class="card-body">
      	 <h5 class="sub-header-title">Trip Details</h5>
       	<table class="table table-bordered mb-0">
      		<tr>
      			<td><strong>Trip Id # :</strong></td>
      			<td>{{ $data->id }}</td>
      			<td><strong>Trip Date: </strong></td>
      			<td>{{$data->created_at->format('d-m-Y')}}</td>
      			<td><strong>Status: </strong></td>
      			<td>
      				@if($data->status=='completed')
        				<span class="badge badge-success">{{ucfirst($data->status)}}</span>
	        			@elseif($data->status=='auto_cancelled')
	        				<span class="badge badge-warning">Auto cancelled</span>
	        			@elseif($data->status=='cancelled_by_driver')
	        				<span class="badge badge-danger">Cancelled by driver</span>
	        			@elseif($data->status=='cancelled_by_customer')
	        				<span class="badge badge-danger">Cancelled by customer</span>
	        			@elseif($data->status=='live')
	        				<span class="badge badge-primary">{{ucfirst($data->status)}}</span>
	        			@endif
         		</td>
      		</tr>
      		<tr>
      			<td><strong>Trip Start Time: </strong></td>
      			<td>{{$data->trip_start_time}}</td>
      			<td><strong>Trip End Time: </strong></td>
      			<td>{{$data->trip_end_time}}</td>
      			<td></td>
      			<td></td>
      		</tr> 		
      	</table>
      	<table class="table table-bordered mt-3">            
            <tbody>
               <tr>
                  <td style="width: 200px;"><strong>From Place :</strong></td>
                  <td>{{$data->tripSearch->from_place}}</td>                  
                  <td style="width: 200px;"><strong>To Place :</strong></td>                  
                  <td>{{$data->tripSearch->to_place}}</td>                  
               </tr>                                  
               <tr>                        
                  <td><strong>From Lattitude :</strong></td>
                  <td>{{$data->tripSearch->from_lat}}</td>
                  <td><strong>From Longitude :</strong></td>
                  <td>{{$data->tripSearch->from_long}}</td>                        
               </tr>
               <tr>                        
                  <td><strong>To Lattitude :</strong></td>
                  <td>{{$data->tripSearch->to_lat}}</td>
                  <td><strong>To Longitude :</strong></td>
                  <td>{{$data->tripSearch->to_long}}</td>                        
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>
<div class="main-content-inner">
   <div class="card">
      <div class="card-body">      	
         <h5 class="sub-header-title">Customer Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Name :</strong></td>
                        <td>{{ucfirst($data->tripSearch->user->name)}}</td>
                        <td><strong>Mobile No :</strong></td>
                        <td>{{$data->tripSearch->user->mobile_number}}</td>                        
                     </tr>
                     <tr>
                        <td><strong>IP Address :</strong></td>
                        <td>{{$data->tripSearch->current_ip}}</td>                  
                        <td></td>                  
                        <td></td>                  
                     </tr>                                  
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-body pt-0">
         <h5 class="sub-header-title">Receiver Details</h5>            
         <div class="single-table">
            <div class="table-responsive">
                <table class="table table-bordered">                 
                  <tbody>
                     <tr>
                        <td><strong>Receiver Name :</strong></td>
                        <td>{{$data->tripSearch->receiver_name}}</td>                  
                        <td><strong>Receiver Number :</strong></td>                  
                        <td>{{$data->tripSearch->receiver_number}}</td>                  
                     </tr>                                  
                     <tr>                                                
                        <td><strong>Goods Type :</strong></td>
                        <td>{{$data->tripSearch->goodsType->name}}</td>                        
                     </tr>                     
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-body pt-2">
         <h5 class="sub-header-title">Driver Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Name :</strong></td>
                        <td>{{ucfirst($data->driverProfile->user->name)}}</td>
                        <td><strong>Mobile No :</strong></td>
                        <td>{{$data->driverProfile->mobile_number}}</td>                        
                     </tr>                                                   
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>   
   <div class="card">
      <div class="card-body pt-2">      	
         <h5 class="sub-header-title">Vehicle Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Vehicle Type :</strong></td>
                        <td>{{$data->vehicleDetails->vehicle->name}}</td>
                        <td><strong>Vehicle No :</strong></td>
                        <td>{{$data->vehicleDetails->vehicle_number}}</td>                        
                     </tr>
                     <tr>
                        <td><strong>Insurance from :</strong></td>
                        <td>{{date('d-m-Y',strtotime($data->vehicleDetails->insurance_from))}}</td>
                        <td><strong>Insurance to:</strong></td>                  
                        <td>{{date('d-m-Y',strtotime($data->vehicleDetails->insurance_to))}}</td>   
                     </tr>                                  
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-body pt-2">      	
         <h5 class="sub-header-title">Payment Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Amount :</strong></td>
                        <td>{{$data->tripSearch->amount}}</td>
                        <td><strong>Payment mode :</strong></td>
                        <td>{{ucfirst($data->tripSearch->payment_type)}}</td>                     
                     </tr>
                     <tr>
                        <td><strong>Coupon code :</strong></td>
                        <td>{{$data->tripSearch->coupon_code}}</td>                  
                        <td></td>                  
                        <td></td>                  
                     </tr>                                  
                  </tbody>
               </table>
            </div>
         </div>
         @if($data->tripSearch->paymentCustomer)
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Txn No :</strong></td>
                        <td>{{$data->tripSearch->paymentCustomer->txn_id}}</td>
                        <td><strong>Payment status :</strong></td>
                        <td>{{$data->tripSearch->paymentCustomer->payment_status}}</td>               
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         @endif
      </div>
   </div>
</div>
@endsection