@extends('layouts.app')

@section('content')
<div class="main-content-inner mt-3">
	<div class="content-wrapper">
		<div class="row">
			<div class="col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="" style="padding: 1rem;">
						<h4 class="header-title">Confirm Trips</h4>
		            <div class="single-table">
		               <div class="table-responsive">
		                  <table class="table table-hover text-center">
	                        <thead class="">
	                           <tr>
											<th>Trip Id#</th>
											<th>Mobile No</th>
											<th style="width: 200px;">From Place</th>
											<th style="width: 200px;">To Place</th>
											<th>Vehicle Type</th>
											<th>Amount / Type</th>
											<th>Date&Time</th>
											<th>Status</th>
											<th></th>
	                           </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($data as $value)
	                        	<tr>
	                        		<td>{{ $value->id}}</td>
	                        		<td>{{ $value->tripSearch->user->mobile_number}}</td>
	                        		<td>{{ $value->tripSearch->from_place}}</td>
	                        		<td>{{ $value->tripSearch->to_place}}</td>
	                        		<td>{{ $value->tripSearch->vehicle->name}}</td>
	                        		<td>{{$value->tripSearch->amount}} - {{ucfirst($value->tripSearch->payment_type)}}</td>
	                        		<td>{{ $value->created_at}}</td>
	                        		<td>
	                        			@if($value->status=='completed')
	                        				<span class="badge badge-success">{{ucfirst($value->status)}}</span>       
	                        			@elseif($value->status=='auto_cancelled')
	                        				<span class="badge badge-warning">Auto cancelled</span>
	                        			@elseif($value->status=='cancelled_by_driver')
	                        				<span class="badge badge-danger">Cancelled by driver</span>
	                        			@elseif($value->status=='cancelled_by_customer')
	                        				<span class="badge badge-danger">Cancelled by customer</span>
	                        			@elseif($value->status=='live')
	                        				<span class="badge badge-primary">{{ucfirst($value->status)}}</span>
	                        			@endif
	                        		</td>
	                        		<td><a href='{{url("trip-confirm/$value->id")}}' class="btn btn-sm btn-primary">View</a></td>
	                        	</tr>
	                        	@endforeach
	                        </tbody>
		                  </table>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
	</div>
</div>
@endsection