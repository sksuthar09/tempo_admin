@extends('layouts.app')

@section('content')
<div class="main-content-inner mt-3">
	<div class="content-wrapper">
		<div class="row">
			<div class="col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="header-title">Search Trips</h4>
		            <div class="single-table">
		                <div class="table-responsive">
		                    <table class="table table-hover text-center">
		                        <thead class="">
		                           <tr>
												<th>Id</th>
												<th>Mobile No</th>
												<th style="width: 200px;">From Place</th>
												<th style="width: 200px;">To Place</th>
												<th>Vehicle Type</th>
												<th>Pay Type</th>
												<th>Date&Time</th>
												<th>Status</th>
												<th></th>
		                           </tr>
		                        </thead>
		                        <tbody>
		                        	@foreach($data as $value)
		                        	<tr>
		                        		<td>{{$value->id}}</td>
		                        		<td>{{$value->user->mobile_number}}</td>
		                        		<td>{{$value->from_place}}</td>
		                        		<td>{{$value->to_place}}</td>
		                        		<td>{{$value->vehicle->name}}</td>
		                        		<td>{{ucfirst($value->payment_type)}}</td>
		                        		<td>{{$value->created_at}}</td>
		                        		<td>
		                        			@if($value->status=='booked')
		                        				<span class="badge badge-success">{{ucfirst($value->status)}}</span>                 			
		                        			@elseif($value->status=='cancelled')
		                        				<span class="badge badge-danger">{{ucfirst($value->status)}}</span>
		                        			@elseif($value->status=='pending')
		                        				<span class="badge badge-warning">{{ucfirst($value->status)}}</span>
		                        			@endif
		                        		</td>
		                        		<td><a href='{{url("trip-search/$value->id")}}' class="btn btn-sm btn-primary">View</a></td>
		                        	</tr>
		                        	@endforeach
		                        </tbody>		  	                     
		                    </table>
		                </div>
		            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection