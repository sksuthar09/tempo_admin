@extends('layouts.app')

@section('content')
<div class="page-title-area">
   <div class="row align-items-center">
      <div class="col-sm-12">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Driver Enquiry</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="index.html">Home</a></li>
                    <li><span>Driver Enquiry</span></li>
                </ul>
            </div>
      </div>      
   </div>
</div>
<div class="main-content-inner">
	<div class="card mt-3">
      <div class="card-body">
       <table class="table table-bordered mb-0">
      		<tr>
      			<td><strong>Request Id # :</strong>
                  @if($errors->any())
                   <div class="alert alert-danger">
                       <p><strong>Opps Something went wrong</strong></p>
                       <ul>
                       @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                       @endforeach
                       </ul>
                   </div>
               @endif
               </td>
      			<td>{{ $data['id'] }}</td>
      			<td><strong>Name :</strong></td>
      			<td>{{ $data['name'] }}</td>
      		</tr>
      		<tr>
      			<td><strong>Enquiry Date: </strong></td>
      			<td>{{$data['created_at']->format('d-m-Y H:i')}}</td>
            <td><strong>Status: </strong></td>
            <td>
              @if($data->status =='review')
              <span class="badge badge-primary">Review</span>
              @elseif($data->status =='pending')
              <span class="badge badge-warning">Pending</span>
              @elseif($data->status =='reject')
              <span class="badge badge-danger">Reject</span>
              @elseif($data->status =='confirm')
              <span class="badge badge-success">Confirm</span>
              @endif
            </td>
      		</tr>
      	</table>
      </div>
   </div>
</div>
<div class="main-content-inner">
   <div class="card">
      <div class="card-body">
      	<!-- <table class="table table-bordered">
      		<tr>
      			<td><strong>Request Id # :</strong></td>
      			<td>{{ $data['id'] }}</td>
      			<td><strong>Name :</strong></td>
      			<td>{{ $data['name'] }}</td>
      		</tr>
      	</table> -->
         <!-- <p><b>Id :</b><span>{{ $data['id'] }}</span></p> 
         <p><b>Name :</b><span>{{ $data['name'] }}</span></p>  -->
         <!-- <h4 class="header-title">Name : {{ $data['name'] }}</h4> -->
         <!-- <hr> -->
         <h5 class="header-title">Personal Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Name :</strong></td>
                        <td>{{ $data['name'] }}</td>
                        <td><strong>Mobile No :</strong></td>
                        <td>{{ $data['mobile_number'] }}</td>                        
                     </tr>
                     <tr>
                      <td><strong>Address :</strong></td>
                        <td>{{$data['address']}}</td>    
                        <td><strong>City :</strong></td>
                        <td>{{$data['city']}}</td>                  
                     </tr>    

                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <form action="{{ url('tempo_enquiry') }}" method="post" enctype='multipart/form-data'>
   @csrf
   <input type="hidden" name="enquiry_id" value="{{$data->id}}">
   <input type="hidden" name="user_id" value="{{$data->users_id}}">
   <input type="hidden" name="drive" value="{{$data->drive}}">
   <div class="card">
      <div class="card-body">         
         <h5 class="header-title">Vehicle Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>                        
                        <td><strong>Vehicle type :</strong></td>
                        <td>{{$data->vehicle->name}}</td>
                        <td><strong>Vehicle No :</strong></td>
                        <td>{{$data->vehicle_number}}</td>                        
                     </tr>
                     <tr>                        
                        <td><strong>Own vehicle :</strong></td>
                        <td>{{$data['own_vehicle'] =='1' ? 'Yes' : 'No'}}</td>
                        <td><strong>Drive the vehicle :</strong></td>
                        <td>{{$data['drive'] =='1' ? 'Yes' : 'No'}}</td>                        
                     </tr>            
                     <tr>                        
                        <td><strong>Vehicle Insurance from :</strong></td>
                        <td>
                           <input class="form-control{{ $errors->has('insurance_from') ? ' has-error' : '' }}" type="date" name="insurance_from" id="example-date-input" value="{{$data->insurance_from}}">                           
                        </td>
                        <td><strong>Vehicle Insurance to :</strong></td>
                        <td><input class="form-control{{ $errors->has('insurance_to') ? ' has-error' : '' }}" type="date" name="insurance_to" id="example-date-input" value="{{$data->insurance_to}}"></td>                   
                     </tr>
                     <tr>                        
                        <td><strong>Date reg :</strong></td>
                        <td><input class="form-control{{ $errors->has('reg_date') ? ' has-error' : '' }}" type="date" name="reg_date" id="example-date-input" value="{{$data->reg_date}}"></td>
                        <td><strong>Reg validity :</strong></td>
                        <td><input class="form-control{{ $errors->has('red_validity') ? ' has-error' : '' }}" type="date" name="red_validity" id="example-date-input" value="{{$data->red_validity}}"></td>                
                     </tr>
                     <tr>                        
                        <td><strong>Engine no :</strong></td>
                        <td><input type="text" name="engine_no" class="form-control{{ $errors->has('engine_no') ? ' has-error' : '' }}" value="{{$data->engine_no}}"></td>
                        <td><strong>Vehicle class :</strong></td>
                        <td><input type="text" name="vehicle_class" class="form-control{{ $errors->has('vehicle_class') ? ' has-error' : '' }}" value="{{$data->vehicle_class}}"></td>     
                     </tr>
                      <tr>                        
                        <td><strong>Fuel used :</strong></td>
                        <td>
                        	<select class="form-control{{ $errors->has('fual_used') ? ' has-error' : '' }}" name="fual_used" id="fual_used">
                        		<option value="">Select</option>
                        		<option value="petrol">Petrol</option>
                        		<option value="diesal">Diesal</option>
                        	</select>
                        </td>                                        
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   @if($data->drive=='0')
   <div class="card">
      <div class="card-body">
         <h5 class="header-title">Driver Details</h5>
         <!-- <hr> -->
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered">                 
                  <tbody>
                     <tr>
                        <td><strong>Name :</strong></td>
                        <td><input type="text" name="driver_name" class="form-control{{ $errors->has('driver_name') ? ' has-error' : '' }}" value="{{$data->driver_name}}"></td>
                        <td><strong>Mobile No :</strong></td>
                        <td><input type="text" name="driver_mobile_number" class="form-control{{ $errors->has('driver_mobile_number') ? ' has-error' : '' }}" value="{{$data->driver_mobile_number}}"></td>        
                     </tr>
                     <tr>
                        <td><strong>Address :</strong></td>
                        <td><input type="text" name="driver_address" class="form-control{{ $errors->has('driver_address') ? ' has-error' : '' }}" value="{{$data->driver_address}}"></td>
                        <td><strong>City :</strong></td>
                        <td><input type="text" name="driver_city" class="form-control{{ $errors->has('driver_city') ? ' has-error' : '' }}" value="{{$data->driver_city}}"></td>
                     </tr>
                     <tr>
                        <td><strong>Pincode :</strong></td>
                        <td><input type="text" name="driver_pincode" class="form-control{{ $errors->has('driver_pincode') ? ' has-error' : '' }}" value="{{$data->driver_pincode}}"></td>
                        <td></td>
                        <td></td>                   
                     </tr>                     
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   @endif
   <div class="card">
      <div class="card-body">
      	<hr>
         <h5 class="header-title">Check list</h5>
         <div class="row">
            <div class="col-md-4">               
               <h6>Owner Documents</h6>
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('owner_doc_pan_card') ? ' has-error' : '' }}">
                  <input type="checkbox" name="owner_doc_pan_card" value="{{$data->owner_doc_pan_card}}" class="custom-control-input" id="customCheck1">
                  <label class="custom-control-label" for="customCheck1">PAN Card</label>
              </div>
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('owner_doc_cancelled_cheque') ? ' has-error' : '' }}">
                  <input type="checkbox" name="owner_doc_cancelled_cheque" value="{{$data->owner_doc_cancelled_cheque}}" class="custom-control-input" id="customCheck2">
                  <label class="custom-control-label" for="customCheck2">Cancelled Cheque or Passbook</label>
              </div>       
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('owner_doc_adhar_card') ? ' has-error' : '' }}">
                  <input type="checkbox" name="owner_doc_adhar_card" class="custom-control-input" value="{{$data->owner_doc_adhar_card}}" id="customCheck3">
                  <label class="custom-control-label" for="customCheck3">Adhar Card</label>
              </div>       
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('owner_doc_address_proof') ? ' has-error' : '' }}">
                  <input type="checkbox" name="owner_doc_address_proof" class="custom-control-input" value="{{$data->owner_doc_address_proof}}" id="customCheck4">
                  <label class="custom-control-label" for="customCheck4">Address Proof</label>
              </div>    
            </div>
            <div class="col-md-4">
               <h6 class="mt-4">Vehicle Documents</h6>
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('vehicle_doc_rc') ? ' has-error' : '' }}">
                  <input type="checkbox" name="vehicle_doc_rc" class="custom-control-input" value="{{$data->vehicle_doc_rc}}" id="customCheck5">
                  <label class="custom-control-label" for="customCheck5">Vehicle RC</label>
              </div>
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('vehicle_doc_permit') ? ' has-error' : '' }}">
                  <input type="checkbox" name="vehicle_doc_permit" class="custom-control-input" value="{{$data->vehicle_doc_permit}}" id="customCheck6">
                  <label class="custom-control-label" for="customCheck6">Vehicle Permit</label>
              </div>       
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('vehicle_doc_insurance') ? ' has-error' : '' }}">
                  <input type="checkbox" name="vehicle_doc_insurance" class="custom-control-input" value="{{$data->vehicle_doc_insurance}}" id="customCheck7">
                  <label class="custom-control-label" for="customCheck7">Vehicle Insurance</label>
              </div> 
            </div>
            <div class="col-md-4">
               <h6 class="mt-4">Driver Documents</h6>
               <div class="custom-control custom-checkbox mt-4{{ $errors->has('driver_doc_licence') ? ' has-error' : '' }}">
                  <input type="checkbox" name="driver_doc_licence" value="{{$data->driver_doc_licence}}" class="custom-control-input" id="customCheck8">
                  <label class="custom-control-label" for="customCheck8">Driver Licence</label>
              </div>
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('driver_doc_adhar_card') ? ' has-error' : '' }}">
                  <input type="checkbox" name="driver_doc_adhar_card" value="{{$data->driver_doc_adhar_card}}" class="custom-control-input" id="customCheck9">
                  <label class="custom-control-label" for="customCheck9">Aadhar Card</label>
              </div>       
              <div class="custom-control custom-checkbox mt-4{{ $errors->has('driver_doc_address') ? ' has-error' : '' }}">
                  <input type="checkbox" name="driver_doc_address" value="{{$data->driver_doc_address}}" class="custom-control-input" id="customCheck12">
                  <label class="custom-control-label" for="customCheck12">Address Proof</label>
              </div>  
            </div>
         </div>                              
      </div>
      <div class="card">
      	<div class="card-body">
				<button class="btn btn-success" type="submit" name="approve" value="approve">Approve</button>&nbsp;&nbsp;       
				<button class="btn btn-warning" type="submit" name="save" value="save">Save & Pending</button>&nbsp;&nbsp;
				<button class="btn btn-danger" type="submit" name="reject" value="reject">Reject</button>
			</div>
		</div>
   </div>
   </form>
</div>
<script type="text/javascript">
   $('#fual_used').val("{{$data->fual_used}}");
   $('input.custom-control-input[value="1"]').prop('checked', true);   
</script>
@endsection
           