@extends('layouts.app')

@section('content')
<div class="main-content-inner mt-3">
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            @if (\Session::has('success'))
               <div class="alert alert-success">
                 <strong>Success!</strong> {!! \Session::get('success') !!}
               </div>               
            @endif
            <h4 class="header-title">Vehicle Enquiry</h4>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table table-hover text-center">
                        <thead class="text-uppercase">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Mobile Number</th>
                                <th>City</th>
                                <th>Vehicle Type</th>
                                <th>Enquiry Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        @foreach($data as $key=>$value)
                        <tbody>
                            <tr>
                            <td> {{$value->id}}</td>
                            <td> {{$value->name}}</td>
                            <td> {{$value->mobile_number}}</td>
                            <td> {{$value->city}}</td>
                            <td> {{$value->vehicle->name}}</td>
                            <td> {{ date('d-m-Y H:i',strtotime($value->created_at)) }}</td>
                            <td> 
                              @if($value->status =='review')
                              <span class="badge badge-primary">Review</span>
                              @elseif($value->status =='pending')
                              <span class="badge badge-warning">Pending</span>
                              @elseif($value->status =='reject')
                              <span class="badge badge-danger">Reject</span>
                              @elseif($value->status =='confirm')
                              <span class="badge badge-success">Confirm</span>
                              @endif
                            </td>                      
                            <td>
                              
                                 <a href='{{ url("tempo_enquiry/$value->id") }}' class="btn btn-sm btn-primary">view</a>
                              
                          </td> 
                       </tr>
                       </tbody>
                       @endforeach
                    </table>
                </div>
                {{ $data->links() }}
            </div>
        </div>
      </div>
  </div>
</div>
@endsection