@extends('layouts.app')

@section('content')
<div class="main-content-inner mt-3">
	<div class="content-wrapper">
		<div class="row">
			<div class="col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="" style="padding: 1rem;">
						<h4 class="header-title">Driver Payout List</h4>
		            <div class="single-table">
		               <div class="table-responsive">
		                  <table class="table table-hover text-left">
	                        <thead class="">
	                           <tr>
											<th>Sr. No</th>
											<th>Driver Name</th>
											<th>Payout Date</th>
											<th>Type</th>
											<th>Earning</th>
											<th>Payout</th>
											<th>Payout Status</th>
											<th></th>											
	                           </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($data as $key=>$value)
	                        		<tr>
	                        			<td>{{++$key}}</td>
	                        			<td>{{ucfirst($value->user->name)}}</td>
	                        			<td>{{$value->payout_date->format('d-m-Y')}}</td>
	                        			<td></td>
	                        			<td>{{$value->earning}}</td>
	                        			<td>{{$value->payout}}</td>
	                        			<td>
	                        				@if($value->payout_status=='0')
	                        					<span class="badge badge-danger">Payout Not Released</span>
	                        				@else
	                        					<span class="badge badge-success">Released</span>
	                        				@endif	                        				
	                        			</td>
	                        			<td>
	                        				<a href='{{ url("driver-payout/$value->id") }}' class="btn btn-sm btn-primary">View</a>
	                        			</td>
	                        		</tr>
	                        	@endforeach
	                        </tbody>
		                  </table>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
	</div>
</div>

@endsection