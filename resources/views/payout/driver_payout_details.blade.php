@extends('layouts.app')

@section('content')
<div class="page-title-area">
   <div class="row align-items-center">
      <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Trip Search</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ url('') }}">Dashboard</a></li>
                    <li><a href="{{ url('trip-search') }}">Trip Search</a></li>
                    
                </ul>
            </div>
      </div>
      <div class="col-sm-6 clearfix">
            
      </div>
   </div>
</div>
<div class="main-content-inner">
	<div class="card mt-3">
      <div class="card-body">
       <table class="table table-bordered mb-0">
      		<tr>
      			<td><strong>Payout Id # :</strong></td>      			      			
               <td></td>
               <td><strong>Payout Status</strong></td>
               <td></td>
      		</tr>
      	</table>
      </div>
   </div>
</div>
<div class="main-content-inner">
   <div class="card">
      <div class="card-body">
         <h5 class="header-title">Driver Payout Details</h5>
         <div class="single-table">
            <div class="table-responsive">
               <table class="table table-bordered text-center">
                  <thead class="">
                     <tr>
                        <th>Sr. No</th>
                        <th>Trip ID</th>
                        <th width="200">From Location</th>
                        <th width="200">To Location</th>                        
                        <th>Payout</th>                                             
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($data as $key=>$value)
                        <tr>
                           <td>{{++$key}}</td>
                           <td>{{$value->trip_id}}</td>
                           <td>{{$value->tripSearch->from_place}}</td>
                           <td>{{$value->tripSearch->to_place}}</td>                               
                           <td>{{$value->tripSearch->amount}}</td>                                 
                        </tr>
                     @endforeach
                     <tr>
                        <td colspan="4"><strong  style="float:right">Total</strong></td>
                        <td>{{$value->tripPayout->payout}}</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>   
      </div>
   </div>
</div>
@endsection