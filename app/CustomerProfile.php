<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerProfile extends Model
{
    protected $table='customer_profile';
    protected $fillable=['name','user_id','mobile_number','adhar_card_no','id_front_img','id_back_img','city','address'];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
