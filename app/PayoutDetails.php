<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutDetails extends Model
{
    protected $table = 'payout_details';
    protected $hidden = [];
    public $timestamps = true;
    protected $dates = ['payout_date'];
    protected $fillable = ['driver_id','payout_date','earning','payout','payout_status'];

    public function user(){
        return $this->hasOne('App\User','id','driver_id');
    }
}
