<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkTripPayout extends Model
{
    protected $table = 'link_trip_payout';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['trip_id','payout_id'];

    public function tripSearch(){      
 	   	return $this->hasOne('App\TripSearch','id','trip_id');
	}

	public function tripPayout(){      
 	   	return $this->hasOne('App\PayoutDetails','id','payout_id');
	}
}
