<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripSearch extends Model
{
    protected $table = 'trip_search';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['customer_id','current_ip', 'vehicle_id','from_place', 'from_lat', 'from_long','to_place','to_lat','to_long','receiver_name','receiver_number','goods_type','amount','coupon_code','status'];

    public function user(){
        return $this->hasOne('App\User','id','customer_id');
    }

    public function vehicle(){
        return $this->hasOne('App\Vehicle','id','vehicle_id');
    }

    public function goodsType(){
        return $this->hasOne('App\GoodsType','id','goods_type');
    }

    public function payment(){
        return $this->belongsTo('App\PaymentOnline','id','payment_type_id');        
    }   

    public function paymentCustomer(){
        return $this->payment()->where('payment_type','=', 1);
    } 
}
