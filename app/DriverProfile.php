<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverProfile extends Model
{
    protected $table='driver_profile';    
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable=['name','user_id','adhar_card_no','id_front_img','id_back_img','city','address','pincode','status'];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function vehicleDetails(){
        return $this->hasOne('App\VehicleDetails','driver_profile_id','id');
    }

    public function bankDetails(){
        return $this->hasOne('App\BankDetails','driver_profile_id','id');
    }

    public function linkOwnerDriver(){
        return $this->hasOne('App\LinkOwnerDriver','driver_id','user_id');
    }

    public function payment(){
        return $this->belongsTo('App\PaymentOnline','id','payment_type_id');        
    }   

    public function drivers(){
        return $this->user()->where('user_type','=', Constants::USER_DRIVER);
    } 
}
