<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleEnquiry extends Model
{    
    protected $table = 'vehicle_enquiry';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['users_id','name', 'mobile_number','email', 'adhar_card_no', 'city','address','own_vehicle','vehicle_type','vehicle_number','drive','owner_id_front_img','owner_id_back_img','rc_front_img','driver_id_front_img','driver_id_back_img','rc_back_img','dl_front_img','dl_back_img','status'];

    public function vehicle(){
        return $this->hasOne('App\Vehicle','id','vehicle_type');
    }    
}
