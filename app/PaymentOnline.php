<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentOnline extends Model
{
    protected $table = 'payment_online';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['payment_type','payment_type_id', 'txn_id','payment_status'];

 	public function paymentType(){      
 	   	return $this->hasOne('App\PaymentType','id','payment_type');
	}
}
