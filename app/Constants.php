<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constants extends Model
{
    const CUSTOMER_TRIP = 1;
    const DRIVER_DUE = 2;
    const PAYOUT_MODE = 3; //days

    const USER_PARTNER = 1;
    const USER_DRIVER = 2;
    const USER_CUSTOMER = 3;
}
