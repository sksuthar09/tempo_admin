<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsType extends Model
{
    protected $table = 'goods_type';
    protected $hidden = [];
    public $timestamps = true;
}
