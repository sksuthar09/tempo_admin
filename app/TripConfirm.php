<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripConfirm extends Model
{
    protected $table = 'trip_confirm';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['trip_search_id','driver_id', 'trip_start_time','trip_end_time', 'trip_status'];

    public function user(){
        return $this->hasOne('App\User','id','driver_id');
    }

    public function tripSearch(){
        return $this->hasOne('App\TripSearch','id','trip_search_id');
    }

    public function driverProfile(){
        return $this->hasOne('App\DriverProfile','id','driver_id');
    }

    // public function vehicleDetails(){
    //     return $this->hasOne('App\VehicleDetails','id','driver_id');
    // }

    public function vehicleDetails(){
        return $this->belongsTo('App\VehicleDetails','driver_id','driver_profile_id');
    }


}
