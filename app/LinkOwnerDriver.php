<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkOwnerDriver extends Model
{
    protected $table = 'link_owner_driver';
    protected $hidden = [];
    public $timestamps = false;
    protected $fillable = ['owner_id','driver_id'];

    public function owner(){
        return $this->hasOne('App\User','id','owner_id');
    }
}
