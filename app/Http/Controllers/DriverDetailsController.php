<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DriverProfile;
use App\Constants;

class DriverDetailsController extends Controller
{
   public function __construct(){
      $this->middleware('auth');
   }

   public function index(){

      $data = DriverProfile::with("user")->orderBy('id','desc')->paginate(15);
      // dd($data);
      return view('Driver_Details/driver_profile',['data'=>$data]);
   }

   public function show($id)
   { 
      $data = DriverProfile::with("user")->where('id',$id)->first();    
      // dd($data);       
      return view('Driver_Details/driver_details',['data'=>$data]);
   }
}
