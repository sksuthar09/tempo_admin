<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\PayoutDetails;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   public function index()
   {
      return view('home');
   }


   public function cron(){

      $PAYOUT_MODE = 3;
      $toDate = date('Y-m-d H:i:s');
      $fromDate =  date('Y-m-d 00:00:00', strtotime('-3 day', strtotime($toDate)));
      
      // Count Earning and Payout
      $payoutResult = DB::select("SELECT tc.driver_id driver_id, CURDATE() payout_date, SUM(ts.amount) as earning , SUM(CASE WHEN ts.payment_type = 'online' THEN ts.amount ELSE 0 END) payout  FROM tempo_admin.trip_confirm as tc LEFT JOIN tempo_admin.trip_search as ts ON ts.id = tc.trip_search_id WHERE tc.status='completed' AND ts.status='booked' AND tc.created_at BETWEEN '$fromDate' AND '$toDate' GROUP BY tc.driver_id");

      if($payoutResult){
         // Count Total Trip
         // ts.id NOT IN (3,4) AND Redis Server
         $tripResult = DB::select("SELECT ts.id trip_id, tc.driver_id driver_id FROM tempo_admin.trip_confirm as tc LEFT JOIN tempo_admin.trip_search as ts ON ts.id = tc.trip_search_id WHERE tc.status='completed' AND ts.status='booked' AND tc.created_at BETWEEN '$fromDate' AND '$toDate'");

         DB::transaction(function () use ($payoutResult,$tripResult) {
            $tripResult = json_decode(json_encode($tripResult), true);         
            foreach ($payoutResult as $key => $value) {      
               $result = new PayoutDetails();
               $result->driver_id = $value->driver_id;
               $result->payout_date = $value->payout_date;
               $result->earning = $value->earning;   
               $result->payout = $value->payout;
               $result->save();

               if(isset($result->id)){
                  // Find driver id
                  $tripFinalData = $this->multiSearch($tripResult, array('driver_id'=>$result->driver_id));
                  // Reolace driver id to payout id                                    
                  $tripData = $this->recursive_change_key($tripFinalData, array('driver_id'=>'payout_id'));
                  
                  foreach($tripData as $key => $value)
                  {
                     $tripData[$key]['payout_id'] = $result->id;
                  }
                      // echo $result->id; exit;              
                  DB::table('link_trip_payout')->insert($tripData);                  
               }
            }
         });
      }     
   }

   function multiSearch(array $array, array $pairs)
    {
        $found = array();
        foreach ($array as $aKey => $aVal) {
            $coincidences = 0;
            foreach ($pairs as $pKey => $pVal) {
                if (array_key_exists($pKey, $aVal) && $aVal[$pKey] == $pVal) {
                    $coincidences++;
                }
            }
            if ($coincidences == count($pairs)) {
                $found[$aKey] = $aVal;
            }
        }

        return $found;
    }

    function recursive_change_key($arr, $set) {
        if (is_array($arr) && is_array($set)) {
         $newArr = array();
         foreach ($arr as $k => $v) {
             $key = array_key_exists( $k, $set) ? $set[$k] : $k;
             $newArr[$key] = is_array($v) ? $this->recursive_change_key($v, $set) : $v;
         }
         return $newArr;
      }
      return $arr;    
    }   
}
