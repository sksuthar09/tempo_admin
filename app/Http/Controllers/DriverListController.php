<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriverListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data=DB::table('driver_details')->get();
        return view('Driver_List/driver_list',['data'=>$data]);
    }
    public function adddriver()
    {
        
        return view('Driver_List/add_driver');
    }

    public function add_driver(Request $request)
    {
      
        DB::table('driver_details')->insert([
            'name' => $request->name, //This Code coming from ajax request
            'mobile_number' => $request->mobile_number, //This Chief coming from ajax request
            'email' => $request->email,
            'address' => $request->address,
            'tempo_type' => $request->tempo_type,
            'contact_date' => $request->contact_date,
        ]);

        return response()->json(['success']);
        
       
    }
    
}
