<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use App\VehicleEnquiry;
use App\DriverProfile;
use App\VehicleDetails;
use App\LinkOwnerDriver;
use App\User;

class VehicleEnquiryController extends Controller
{
   public function __construct()
   {
      $this->middleware('auth');
   }

   public function index()
   {
      $data = VehicleEnquiry::with('vehicle')->orderBy('id','desc')->paginate(15);      
      return view('VehicleEnquiry.index',['data'=>$data]);
   }

   public function show($id)
   { 
      // $data = VehicleEnquiry::with('vehicle')->where('id',$id)->where('status','!=','confirm')->first(); 
      $data = VehicleEnquiry::with('vehicle')->where('id',$id)->first();
      // dd($data);
      if($data){
         return view('VehicleEnquiry.details',['data'=>$data]);
      }else{
         return redirect()->back()->with('success', 'Already confirmed');  
      }           
   }

   public function store(Request $request){
      $post = $request->all();            
      // print_r($post); exit;
      if(isset($post['approve'])){         
         $validator = $request->validate([
            'drive' => ['required'],
            'insurance_from' => ['required'],
            'insurance_to' => ['required'],
            'reg_date' => ['required'],
            'red_validity' => ['required'],
            'engine_no' => ['required'],
            'vehicle_class' => ['required'],
            'fual_used' => ['required'],
            'driver_name'=>['sometimes','required'], 
            'driver_mobile_number'=>['sometimes','required','digits:10'],
            'driver_address'=>['sometimes','required'],
            'driver_city'=>['sometimes','required'],
            'driver_pincode'=>['sometimes','required','digits:6'],
            'owner_doc_pan_card' => ['required'],
            'owner_doc_cancelled_cheque' => ['required'],
            'owner_doc_adhar_card' => ['required'],
            'owner_doc_address_proof' => ['required'],
            'driver_doc_licence' => ['required'],
            'driver_doc_adhar_card' => ['required'],
            'driver_doc_address' => ['required'],       
            'vehicle_doc_rc' => ['required'],
            'vehicle_doc_permit' => ['required'],
            'vehicle_doc_insurance' => ['required']                     
         ]);
         
         DB::transaction(function () use ($post) {

            $VehicleEnquiry = VehicleEnquiry::where('id',$post['enquiry_id'])->where('id','!=','confirm')->first();
            if($VehicleEnquiry){
               $driverUserId = $post['user_id'];
               // Get new user id if owner changes
               if($post['drive']=='0'){
                  $user = new User();
                  $user->name = $post['driver_name'];
                  $user->mobile_number = $post['driver_mobile_number']; // will send otp on register
                  $user->user_type = '2';
                  $user->save();
                  
                  $driverUserId = $user->id;

                  $LinkOwnerDriver = new LinkOwnerDriver();
                  $LinkOwnerDriver->owner_id = $post['user_id'];
                  $LinkOwnerDriver->driver_id = $user->id;
                  $LinkOwnerDriver->save();
               }

               
               
               $VehicleEnquiry->driver_name = $post['driver_name'];
               $VehicleEnquiry->driver_mobile_number = $post['driver_mobile_number'];
               $VehicleEnquiry->driver_address = $post['driver_address'];
               $VehicleEnquiry->driver_city = $post['driver_city'];
               $VehicleEnquiry->driver_pincode = $post['driver_pincode'];

               $VehicleEnquiry->insurance_from = $post['insurance_from'];
               $VehicleEnquiry->insurance_to = $post['insurance_to'];
               $VehicleEnquiry->reg_date = $post['reg_date'];
               $VehicleEnquiry->red_validity = $post['red_validity'];
               $VehicleEnquiry->engine_no = $post['engine_no'];
               $VehicleEnquiry->vehicle_class = $post['vehicle_class'];
               $VehicleEnquiry->fual_used = $post['fual_used'];       
               $VehicleEnquiry->owner_doc_pan_card = isset($post['owner_doc_pan_card']) ? '1' : '0';
               $VehicleEnquiry->owner_doc_cancelled_cheque = isset($post['owner_doc_cancelled_cheque']) ? '1' : '0';
               $VehicleEnquiry->owner_doc_adhar_card = isset($post['owner_doc_adhar_card']) ? '1' : '0';
               $VehicleEnquiry->owner_doc_address_proof = isset($post['owner_doc_address_proof']) ? '1' : '0';
               $VehicleEnquiry->vehicle_doc_rc = isset($post['vehicle_doc_rc']) ? '1' : '0';
               $VehicleEnquiry->vehicle_doc_permit = isset($post['vehicle_doc_permit']) ? '1' : '0';
               $VehicleEnquiry->vehicle_doc_insurance = isset($post['vehicle_doc_insurance']) ? '1' : '0';
               $VehicleEnquiry->driver_doc_licence = isset($post['driver_doc_licence']) ? '1' : '0';
               $VehicleEnquiry->driver_doc_adhar_card = isset($post['driver_doc_adhar_card']) ? '1' : '0';
               $VehicleEnquiry->driver_doc_address = isset($post['driver_doc_address']) ? '1' : '0';             
               $VehicleEnquiry->status = 'confirm';
               $VehicleEnquiry->save();

               $DriverProfile = new DriverProfile();
               $DriverProfile->user_id = $driverUserId;
               $DriverProfile->address = $post['driver_address'];
               $DriverProfile->city = $post['driver_city'];
               $DriverProfile->pincode = $post['driver_pincode'];
               $DriverProfile->adhar_card_no = $VehicleEnquiry->adhar_card_no;
               $DriverProfile->id_front_img = $VehicleEnquiry->id_front_img;
               $DriverProfile->id_back_img = $VehicleEnquiry->id_back_img;
               $DriverProfile->city = $VehicleEnquiry->city;
               $DriverProfile->address = $VehicleEnquiry->address;               
               $DriverProfile->save();

               $VehicleDetails = new VehicleDetails();
               $VehicleDetails->driver_profile_id = $DriverProfile->id;
               $VehicleDetails->vehicle_id = $VehicleEnquiry->vehicle_type;
               $VehicleDetails->vehicle_number = $VehicleEnquiry->vehicle_number;
               $VehicleDetails->rc_front_img = $VehicleEnquiry->rc_front_img;
               $VehicleDetails->rc_back_img = $VehicleEnquiry->rc_back_img;
               $VehicleDetails->dl_front_img = $VehicleEnquiry->dl_front_img;
               $VehicleDetails->dl_back_img = $VehicleEnquiry->dl_back_img;
               if( $VehicleEnquiry->drive=='0'){
                  $VehicleDetails->owner_id_front_img = $VehicleEnquiry->owner_id_front_img;
                  $VehicleDetails->owner_id_back_img = $VehicleEnquiry->owner_id_back_img;                
               }
               // $VehicleDetails->driver_id_front_img = $VehicleEnquiry->driver_id_front_img;
               // $VehicleDetails->driver_id_back_img = $VehicleEnquiry->driver_id_back_img;

               $VehicleDetails->insurance_from = $post['insurance_from'];
               $VehicleDetails->insurance_to = $post['insurance_to'];
               $VehicleDetails->reg_date = $post['reg_date'];
               $VehicleDetails->red_validity = $post['red_validity'];
               $VehicleDetails->engine_no = $post['engine_no'];
               $VehicleDetails->vehicle_class = $post['vehicle_class'];
               $VehicleDetails->fual_used = $post['fual_used'];
               $VehicleDetails->owner_doc_pan_card = isset($post['owner_doc_pan_card']) ? '1' : '0';
               $VehicleDetails->owner_doc_cancelled_cheque = isset($post['owner_doc_cancelled_cheque']) ? '1' : '0';
               $VehicleDetails->owner_doc_adhar_card = isset($post['owner_doc_adhar_card']) ? '1' : '0';
               $VehicleDetails->owner_doc_address_proof = isset($post['owner_doc_address_proof']) ? '1' : '0';
               $VehicleDetails->vehicle_doc_rc = isset($post['vehicle_doc_rc']) ? '1' : '0';
               $VehicleDetails->vehicle_doc_permit = isset($post['vehicle_doc_permit']) ? '1' : '0';
               $VehicleDetails->vehicle_doc_insurance = isset($post['vehicle_doc_insurance']) ? '1' : '0';
               $VehicleDetails->driver_doc_licence = isset($post['driver_doc_licence']) ? '1' : '0';
               $VehicleDetails->driver_doc_adhar_card = isset($post['driver_doc_adhar_card']) ? '1' : '0';
               $VehicleDetails->driver_doc_address = isset($post['driver_doc_address']) ? '1' : '0';   
               $VehicleDetails->save();                          
            }
         });                  
         return redirect('tempo_enquiry')->with('success', 'Request Id : '.$post['enquiry_id'].' confirmed successfully');            
      }

      if(isset($post['save'])){
         
         DB::transaction(function () use ($post) {

            $VehicleEnquiry = VehicleEnquiry::where('id',$post['enquiry_id'])->where('id','!=','confirm')->first();

            if($VehicleEnquiry){
               $VehicleEnquiry->insurance_from = $post['insurance_from'];
               $VehicleEnquiry->insurance_to = $post['insurance_to'];
               $VehicleEnquiry->reg_date = $post['reg_date'];
               $VehicleEnquiry->red_validity = $post['red_validity'];
               $VehicleEnquiry->engine_no = $post['engine_no'];
               $VehicleEnquiry->vehicle_class = $post['vehicle_class'];
               $VehicleEnquiry->fual_used = $post['fual_used'];
               $VehicleEnquiry->owner_doc_pan_card = isset($post['owner_doc_pan_card']) ? '1' : '0';
               $VehicleEnquiry->owner_doc_cancelled_cheque = isset($post['owner_doc_cancelled_cheque']) ? '1' : '0';
               $VehicleEnquiry->owner_doc_adhar_card = isset($post['owner_doc_adhar_card']) ? '1' : '0';
               $VehicleEnquiry->owner_doc_address_proof = isset($post['owner_doc_address_proof']) ? '1' : '0';
               $VehicleEnquiry->vehicle_doc_rc = isset($post['vehicle_doc_rc']) ? '1' : '0';
               $VehicleEnquiry->vehicle_doc_permit = isset($post['vehicle_doc_permit']) ? '1' : '0';
               $VehicleEnquiry->vehicle_doc_insurance = isset($post['vehicle_doc_insurance']) ? '1' : '0';
               $VehicleEnquiry->driver_doc_licence = isset($post['driver_doc_licence']) ? '1' : '0';
               $VehicleEnquiry->driver_doc_adhar_card = isset($post['driver_doc_adhar_card']) ? '1' : '0';
               $VehicleEnquiry->driver_doc_address = isset($post['driver_doc_address']) ? '1' : '0';                        
               $VehicleEnquiry->status = 'pending';
               $VehicleEnquiry->save();
            }
         });

         return redirect('tempo_enquiry')->with('success', 'Request Id : '.$post['enquiry_id'].' updated successfully');
      }

      if(isset($post['reject'])){
         
      }
   }
}
