<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Vehicle;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data=DB::table('vehicle')->get();
        return view('Vehicle.index',['data'=>$data]);
    }

    public function show($id)
    { 
        $data = DB::table('vehicle')->where('id', $id)->get();
        
        return view('Vehicle.details',['data'=>$data]);
    }
    public function test(Request $request)
    {
        $post=new Tempo;
        $post = Tempo::find($request->id);
        $post->status = $request->status;
        $post->save();
        return response()->json(['status'=>$post['status']]);
     }
}
