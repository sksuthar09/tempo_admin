<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\LiveTrip;
use App\TripSearch;
use App\TripConfirm;

class TripController extends Controller
{
	public function __construct(){
   		$this->middleware('auth');
	}

	public function tripSearch(){
		$data = TripSearch::orderBy('id','desc')->paginate(15);
		return view('trip.trip_search',['data'=>$data]);
	}

	public function tripSearchDetails($id){
		$data = TripSearch::where('id',$id)->first();
		return view('trip.trip_search_details',['data'=>$data]);
	}

	public function tripConfirm(){
		$data = TripConfirm::with('tripSearch')->orderBy('id','desc')->paginate(15);
		// dd($data);
		return view('trip.trip_confirm',['data'=>$data]);
	}

	public function tripConfirmDetails($id){
		$data = TripConfirm::with('tripSearch.paymentCustomer')->where('id',$id)->first();
		return view('trip.trip_confirm_details',['data'=>$data]);
	}


	public function liveTripIndex(Request $request)
    {
    	 event(new LiveTrip('huu'));
    	return view('live-trip.index');
    }

	public function liveTrip(Request $request)
    {
        $message = $request->input('message', '');

        $post = $request->all();

        if (strlen($post['message'])) {        	
            event(new LiveTrip('huu'));
        }
    }
}
