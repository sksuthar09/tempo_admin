<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PayoutDetails;
use App\LinkTripPayout;

class PaymentController extends Controller
{
    
	public function driverPayout(){
		$data = PayoutDetails::orderBy('id','desc')->paginate(15);
		// dd($data);
		return view('payout.driver_payout_list',['data'=>$data]);
	}

	public function driverPayoutDetails($id){
		$data = LinkTripPayout::with('tripSearch')->where('payout_id',$id)->get();		
		// $data = PayoutDetails::orderBy('id','desc')->first();
		return view('payout.driver_payout_details',['data'=>$data]);
	}
}
