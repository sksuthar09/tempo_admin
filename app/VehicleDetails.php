<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleDetails extends Model
{
    protected $table = 'vehicle_details';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['driver_profile_id','vehicle_id', 'vehicle_number','rc_front_img', 'rc_back_img','dl_front_img','dl_back_img','insurance_from','insurance_to','reg_date','red_validity','engine_no','vehicle_class','fual_used','owner_id_front_img','owner_id_back_img'];

    public function vehicle(){
        return $this->hasOne('App\Vehicle','id','vehicle_id');
    }
}
