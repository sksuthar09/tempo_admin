<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    protected $table='bank_details';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable=['driver_profile_id','beneficiary_name','beneficiary_bank','beneficiary_account_no','beneficiary_ifsc','beneficiary_branch','beneficiary_status'];

    public function bank(){
        return $this->hasOne('App\Bank','id','beneficiary_bank');
    }
}
