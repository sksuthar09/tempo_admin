<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    protected $table = 'payment_type';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['payment_name','payment_code'];
}
