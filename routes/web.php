<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/cron', 'HomeController@cron');




Route::get('/home', 'HomeController@index')->name('home');

Route::group(array('prefix' => 'trip-search/'), function() {		
	Route::get('/', 'TripController@tripSearch');
	Route::get('/{id}', 'TripController@tripSearchDetails');
});

Route::group(array('prefix' => 'trip-confirm/'), function() {		
	Route::get('/', 'TripController@tripConfirm');
	Route::get('/{id}', 'TripController@tripConfirmDetails');
});

Route::group(array('prefix' => 'driver-payout/'), function() {
	Route::get('/', 'PaymentController@driverPayout');
	Route::get('/{id}', 'PaymentController@driverPayoutDetails');
});


Route::resource('Tempo/tempo', 'VehicleController');


Route::resource('Driver_List/driver_list', 'DriverListController');
Route::view('Driver_List/add_driver','Driver_List/add_driver');
Route::post('Driver_List/add_driver','DriverListController@add_driver');

Route::resource('tempo_enquiry', 'VehicleEnquiryController');


Route::resource('Driver_Details/driver_profile', 'DriverDetailsController');


//-----live trip------//
Route::get('live-trip', 'TripController@liveTripIndex');
Route::post('/api/live-trip', 'TripController@liveTrip');